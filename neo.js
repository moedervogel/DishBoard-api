const neo4j = require('neo4j-driver')

const uri = process.env.NEO4J_URL;
const user = process.env.NEO4J_USER;
const password = process.env.NEO4J_PASSWORD;

function connect(dbName) {

    console.log("URL: " + uri + " User: " + user + " Password: " + password)
    this.dbName = dbName
    this.driver = neo4j.driver(
        uri,
        neo4j.auth.basic(user, password)
    )
}

function session() {
    return this.driver.session({
        database: this.dbName,
        defaultAccessMode: neo4j.session.WRITE
    })
}

module.exports = {
    connect,
    session,
    getFriends: 'MATCH (you {name:"You"})-[:FRIEND]->(yourFriends) RETURN you, yourFriends',
    followUser: 'MERGE(followingUser:User {id: $followingUserId, email: $followingUserEmail}) MERGE(user:User {id: $userId, email: $userEmail}) MERGE (user)-[:FOLLOWING]->(followingUser) RETURN user,followingUser',
    getFollowingUser: 'MATCH (p:User {id:$userId})-[:FOLLOWING]->(user) RETURN user',
    addUser:'MERGE(user:User {id: $userId, email: $userEmail})',
    dropAll: 'MATCH (n) DETACH DELETE n',
}