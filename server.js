const config = require('./config')

const connect = require('./connect')

const app = require('./src/app')

// the order of starting the app and connecting to the database does not matter
// since mongoose buffers queries till there is a connection

// start the app
const port = config.PORT;

app.listen(port, () => {
    console.log(`server is listening on port ${port}`)
})
// connect to the databases
connect.mongo(process.env.MONGO_DB)
connect.neo(process.env.NEO4J_PROD_DB)
