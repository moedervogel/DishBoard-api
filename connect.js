const mongoose = require('mongoose');
const neo_driver = require('./neo')

const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true
}

async function mongo(dbName) {
    if(dbName == process.env.MONGO_TEST_DB){
		try {
			await mongoose.connect(`${dbName}`, options);
			console.log(`a test connection to mongo DB ${dbName} established`);
		} catch (err) {
			console.error(err);
		}
	} else{
		try {
			await mongoose.connect(`${dbName}`, options);
			console.log(`connection to mongo DB ${dbName} established`);
		} catch (err) {
			console.error(err);
		}
	}
}

function neo(dbName) {
    try {
        neo_driver.connect(dbName)
        console.log(`connection to neo DB ${dbName} established`)
    } catch (err) {
        console.error(err)
    }
}

module.exports = {
    mongo,
    neo,
}
