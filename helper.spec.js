require('dotenv').config()

const connect = require('./connect')

const User = require('./src/models/user.model')() // note we need to call the model caching function
const Recipe = require('./src/models/recipe.model')() // note we need to call the model caching function
const Ingredient = require('./src/models/ingredient.model')() // note we need to call the model caching function
const RecipeBook = require('./src/models/recipeBook.model')() // note we need to call the model caching function

const neo = require('./neo')

// connect to the databases
connect.mongo(process.env.MONGO_TEST_DB)
connect.neo(process.env.NEO4J_TEST_DB)

beforeEach(async () => {
    // drop both collections before each test
    await Promise.all([User.deleteMany(), Recipe.deleteMany(),Ingredient.deleteMany(),RecipeBook.deleteMany()])

    //clear neo db before each test
    const session = neo.session()
    await session.run(neo.dropAll)
    await session.close()
});