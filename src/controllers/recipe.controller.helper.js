
validateRecipeDescriptionAndIngredient = async (req,res,next) => {
    const description = req.body.description;
    const ingredients = req.body.ingredients;
    console.log(req.body.description.length)
    if(!(req.body.description.length > 0)){
        res.status(400).json({error: "Steps needs to be larger then 0"})
    } else if(!(req.body.ingredients.length >= 1)){
        res.status(400).json({error:"Ingredients list needs to be larger then 1"})
    }else{
        next();
    }
}

module.exports = {
    validateRecipeDescriptionAndIngredient,
}