const express = require("express");
const jwt = require("jsonwebtoken");
const { restart } = require("nodemon");
const router = express.Router();
const User = require("../models/user.model")();
const neo = require('../../neo')

var tokenSecret = "secret";

class UserControllerHelper {
    
    constructor(User) {
        this.User = User
    }

    signup = async (req, res,next) =>
    {
        let user = await new this.User(req.body)
        if(!req.body.password || !req.body.email || !req.body.firstName || !req.body.lastName || !req.body.dateOfBirth){
            res.status(400).json({error: "Please complete User data"})
        }else if(await this.User.findOne({email:req.body.email})){
            res.status(409).json({error: "Email already exists"})
        }else{
            await user.save();
            const userInfo = {
                user,
                token:jwt.sign(user.email,tokenSecret)
            }
            const session = neo.session();

            try{
                console.log(user._id.toString())
                console.log(user.email.toString())
                await session.run('MERGE(user:User {id: $userId, email: $userEmail, firstname: $firstName, lastName:$lastName,dateOfBirth:$dateOfBirth})',{
                    userId: user._id.toString(),
                    userEmail: user.email.toString(),
                    firstName: user.firstName.toString(),
                    lastName: user.lastName.toString(),
                    dateOfBirth: user.dateOfBirth.toString()
                })
                session.close()
                res.status(200).send(userInfo)
            }catch(error){
                console.log("The following error ocurred", error)
                res.status(400).json({Error: error + "Neo niet gelukt"})
            }
        }
    }

    login = async(req,res,next) =>
    {
        if(!req.body.email || !req.body.password ){
            res.status(400).json({error: "please fill in Email and password"})
        }else{
            let user = await this.User.findOne({email:req.body.email, password:req.body.password});
            if(!user){
                res.status(400).json({error: "User not found"})
            }else{
                const userInfo = {
                    user,
                    token:jwt.sign(user.email, tokenSecret)
                }
                res.status(200).send(userInfo)
            }
        }
    }

    validateToken(req,res,next){
        const authHeader = req.headers.authorization
        if(!authHeader){
            console.log("Authorization header is missing")
            res.status(401).json({
                error: "Authorization in the header is missing",
            })
        }else{
            const token = authHeader.substring(7, authHeader.length)
            jwt.verify(token, tokenSecret, (err, payload) => {
                if(err){
                    console.log("Not authorized")
                    res.status(401).json({
                        error: "Not authorized"
                    })
                }
                if(payload){
                    console.log("Token is valid", payload)
                    next()
                }
            })
        }
    }


    async followUser(req,res){
        const loggedInUser = await User.findOne({_id:req.body.userId})
    
        if(!loggedInUser){
            res.status(404).json({error:`User with name  ${req.body.email} not found `});
        }
        console.log(req.body.userId)
        console.log(req.params.id)
        const followUser = await User.findOne({_id:req.params.id});
        if(!followUser){
            res.status(404).json({error:`User with email ${req.params.id}`})
        }
    
        const session = neo.session();
    
        console.log("Following user",followUser._id,followUser.email)
        console.log("From user",loggedInUser._id,loggedInUser.email)
    
        let result = '';
        try {
           result = await session.run(neo.followUser,{
                followingUserId:followUser._id.toString(),
                followingUserEmail:followUser.email.toString(),
                userId:loggedInUser._id.toString(),
                userEmail:loggedInUser.email.toString(),
            })
        } catch (error) {
            console.log("TEST", error);
        }
        
        const userIds = []
        result.records.forEach(record => {
            console.log(record);
            userIds.push(record.get('user').properties.id);
            userIds.push(record.get('followingUser').properties.id);
        });
        console.log(userIds)
        res.status(200).json(userIds)
    }

    getOne = async (id) => {
        console.log("Hij komt in de getOne()")
        const entity = await this.User.findById(id)
        return entity;
    }
    
    getFollowing = async (req,res) =>{
        const loggedInUser = await User.findOne({_id:req.params.id})
        const users = []

        if(!loggedInUser){
            res.status(400).json({error:`User with id  ${req.params.id} not found `});
        }
        console.log("Logged User",loggedInUser._id)
        const session = neo.session();
    
        let result = '';
        try {
           result = await session.run(neo.getFollowingUser,{
                userId:req.params.id
            })
        } catch (error) {
            console.log("TEST", error);
        }
        console.log(result.records);
        for(let i = 0; i < result.records.length; i++){
            console.log("Number:", i ," ",result.records[i]);
            users.push(await this.User.findById(result.records[i].get('user').properties.id));
        }
        console.log(users)
        res.status(200).json(users)
    }
    
}

module.exports = UserControllerHelper;