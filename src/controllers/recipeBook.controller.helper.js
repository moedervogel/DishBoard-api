const express = require("express");
const jwt = require("jsonwebtoken");
const { restart } = require("nodemon");
const router = express.Router();
const RecipeBook = require("../models/recipeBook.model")();
const neo = require('../../neo')

var tokenSecret = "secret";
class RecipeBookControllerHelper {
        
    constructor(RecipeBook) {
        this.RecipeBook = RecipeBook
    }
    getRecipeBookFromUserId = async (req,res,next) => {
        console.log(req.params.id)
        const recipeBook = await this.RecipeBook.find({user:req.params.id})
        res.status(201).json(recipeBook)
    }

    addToNeo4j = async (req,res,next) => {
        const session = neo.session();
        try{
            console.log(req.body)
            console.log(req.body.titel)
            console.log(req.body.about)
            console.log(req.body.chapters)
            console.log(req.body.user)
            
            await session.run('MERGE(recipeBook:RecipeBook {title: $title, about: $about})',{
                title: req.body.title,
                about: req.body.about,
            })
            await session.run('MATCH(user:User),(recipeBook:RecipeBook) WHERE user.email = $userEmail AND recipeBook.title = $title CREATE (user)-[:CREATES]->(recipeBook)',{
                userEmail: req.body.user.email,
                title: req.body.title
            })
            for(let chapter of req.body.chapters){
                await session.run('MERGE(chapter:Chapter {chapterTitle: $chapterTitle ,chapterIntro: $chapterIntro})',{
                    chapterTitle:chapter.chapterTitle,
                    chapterIntro:chapter.chapterIntro
                })
                await session.run('MATCH(recipeBook:RecipeBook),(chapter:Chapter) WHERE recipeBook.title = $title AND chapter.chapterTitle = $chapterTitle CREATE (chapter)-[:PART_OF]->(recipeBook)',{
                    title: req.body.title,
                    chapterTitle:chapter.chapterTitle
                })
            }
            session.close()
            next()
        }catch(error){
            console.log("The following error ocurred", error)
            res.status(400).json({Error: error + "Neo niet gelukt"})
        }
    }

    followRecipeBook = async (req,res,next) => {
        console.log(req.body.email)
        
        const recipeBook = await this.RecipeBook.findById(req.params.id)
        const session = neo.session();
        console.log(recipeBook.title)
        try{
            await session.run('MATCH(user:User),(recipeBook:RecipeBook) WHERE user.email = $user AND recipeBook.title = $recipeBook CREATE (user)-[:FOLLOWS]->(recipeBook)',{
                user:req.body.email,
                recipeBook: recipeBook.title
            })
            res.status(201).send(req.body);
        }catch(error){
            console.log("The following error ocurred", error)
            res.status(400).json({Error: error + "Neo niet gelukt"})
        }
    }

    unFollowRecipeBook = async (req,res,next) => {
        console.log(req.body.email)
        
        const recipeBook = await this.RecipeBook.findById(req.params.id)
        const session = neo.session();
        console.log(recipeBook.title)
        try{
            await session.run('MATCH(user:User{email:$user})-[r:FOLLOWS]->(recipeBook:RecipeBook{title:$recipeBook}) DELETE r',{
                user:req.body.email,
                recipeBook: recipeBook.title
            })
            res.status(201).send({succes:'It worked!'});
        }catch(error){
            console.log("The following error ocurred", error)
            res.status(400).json({Error: error + "Neo niet gelukt"})
        }
    }


    getFollowingRecipeBook = async (req,res,next) => {
        const recipeBooks = []
        const session = neo.session();
        let result = '';
        try {
           result = await session.run('MATCH (p:User {id:$userId})-[:FOLLOWS]->(RecipeBook) RETURN RecipeBook',{
                userId:req.params.id
            })
        } catch (error) {
            console.log("TEST", error);
            res.status(400).json({Error: error + "Neo niet gelukt"})
        }
        
        for(let i = 0; i < result.records.length; i++){
            let atitle = result.records[i].get('RecipeBook').properties.title;
            recipeBooks.push(await this.RecipeBook.find({title:atitle}))
        }
        let sendBooks = []
        for(let k = 0; k < recipeBooks.length; k++){
            sendBooks.push(recipeBooks[k][0])
        }
        res.status(200).send(sendBooks)
    }
}


module.exports = RecipeBookControllerHelper;
