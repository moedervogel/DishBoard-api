const express = require('express')
const router = express.Router()

const User = require('../models/user.model')() // note we need to call the model caching function

const CrudController = require('../controllers/crud')
const UserControllerHelper = require('../controllers/user.controller.helper');
const UserCrudController = new CrudController(User)
const UserControllerHelperClass = new UserControllerHelper(User);



// create a user
router.post('/', UserCrudController.create)

// get all users
router.get('/', UserCrudController.getAll)

// get a user
router.get('/:id', UserCrudController.getOne)

// update a user
router.put('/:id', UserCrudController.update)

// remove a user
router.delete('/:id', UserCrudController.delete)

router.post('/register', UserControllerHelperClass.signup)
router.post('/login', UserControllerHelperClass.login)


module.exports = router