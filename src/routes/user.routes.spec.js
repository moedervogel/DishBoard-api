const chai = require('chai');
const expect = chai.expect;

const requester = require('../../requester.spec')
const User = require('../models/user.model')()

describe('user endpoints', function() {
    describe('integration tests', function(){
        it('POST /user/register Should create a new user', async function() {
            const testUser = new User({
                    email:"luuk-bartels@hotmail.com",
                    password:"LuukIsAmazing34" ,
                    firstName:"Luuk",
                    lastName: "Bartels",
                    about: "This is a great bio!",
                    dateOfBirth:"1997-02-16",
                    role:"ADMIN"
            })
            const res = await requester.post('/user/register').send({
                email:testUser.email,
                password:testUser.password,
                firstName:testUser.firstName,
                lastName:testUser.lastName,
                about:testUser.about,
                dateOfBirth:testUser.dateOfBirth,
                role:testUser.role
            })
            expect(res).to.have.status(200)
            const user = await User.findOne({email: testUser.email})
            expect(user).to.have.property('email', testUser.email)
        })
        it('POST /user/register Should not create user with missing email', async function() {
            const testUser = new User({
                    password:"LuukIsAmazing34" ,
                    firstName:"Luuk",
                    lastName: "Bartels",
                    about: "This is a great bio!",
                    dateOfBirth:"1997-02-16",
                    role:"ADMIN"
            })

            const res = await requester.post('/user/register').send({
                password:testUser.password,
                firstName:testUser.firstName,
                lastName:testUser.lastName,
                about:testUser.about,
                dateOfBirth:testUser.dateOfBirth,
                role:testUser.role
            })
            expect(res).to.have.status(400)
            expect(res).to.not.have.property('_id');
            const docCount = await User.find().countDocuments()
            expect(docCount).to.equal(0)
        })
        it('POST /user/register Should not create a user with duplicate email', async function() {
            const testUser = new User({
                    email:"luuk-bartels@hotmail.com",
                    password:"LuukIsAmazing34" ,
                    firstName:"Luuk",
                    lastName: "Bartels",
                    about: "This is a great bio!",
                    dateOfBirth:"1997-02-16",
                    role:"ADMIN"
            })
            const testUser2 = new User({
                email:"luuk-bartels@hotmail.com",
                password:"LuukIsAmazing34" ,
                firstName:"Jasper",
                lastName: "Bartels",
                about: "This is a great bio!",
                dateOfBirth:"1997-02-16",
                role:"USER"
            })
            const res = await requester.post('/user/register').send({
                email:testUser.email,
                password:testUser.password,
                firstName:testUser.firstName,
                lastName:testUser.lastName,
                about:testUser.about,
                dateOfBirth:testUser.dateOfBirth,
                role:testUser.role
            })
            const res2 = await requester.post('/user/register').send({
                email:testUser2.email,
                password:testUser2.password,
                firstName:testUser2.firstName,
                lastName:testUser2.lastName,
                about:testUser2.about,
                dateOfBirth:testUser2.dateOfBirth,
                role:testUser2.role
            })
            expect(res).to.have.status(200)
            expect(res2).to.have.status(409)
            const docCount = await User.find().countDocuments()
            expect(docCount).to.equal(1)
        })
        it('POST /user/login Should login user', async function(){
            const testUser = new User({
                email:"luuk-bartels@hotmail.com",
                password:"LuukIsAmazing34" ,
                firstName:"Luuk",
                lastName: "Bartels",
                about: "This is a great bio!",
                dateOfBirth:"1997-02-16",
                role:"ADMIN"
            })

            await new User({
                email:testUser.email,
                password:testUser.password ,
                firstName:testUser.firstName,
                lastName: testUser.lastName,
                about: testUser.about,
                dateOfBirth:testUser.dateOfBirth,
                role:testUser.role
            }).save()

            const res = await requester.post('/user/login').send({
                "email": testUser.email,
                "password": testUser.password
            })
            expect(res).to.have.status(200)
            const loginUser = res.body;
            expect(loginUser).to.have.key("token",'user')
            
        })
        it('GET /user/ Should get list of all users', async function(){
            const emailLuuk = "luuk-bartels@hotmail.com";
            const emailJasper = "jasper-bartels@hotmail.com";

            await new User({
                email:emailLuuk,
                password:"LuukIsAmazing34" ,
                firstName:"Luuk",
                lastName: "Bartels",
                about: "This is a great bio!",
                dateOfBirth:"1997-02-16",
                role:"ADMIN"
            }).save()
            await new User({
                email:emailJasper,
                password:"LuukIsAmazing34" ,
                firstName:"Jasper",
                lastName: "Bartels",
                about: "This is a great bio!",
                dateOfBirth:"1997-02-16",
                role:"USER"
            }).save()

            const res = await requester.get('/user')
            expect(res).to.have.status(200)
            const users = res.body;
            expect(users[0]).to.have.property('email',emailLuuk)
            expect(users[1]).to.have.property('email',emailJasper)
        })
        it('GET /user/:id Should get user with id', async function(){
            const emailLuuk = "luuk-bartels@hotmail.com";

            const testUser = await new User({
                email:emailLuuk,
                password:"LuukIsAmazing34" ,
                firstName:"Luuk",
                lastName: "Bartels",
                about: "This is a great bio!",
                dateOfBirth:"1997-02-16",
                role:"ADMIN"
            }).save()

            const res = await requester.get(`/user/${testUser._id}`)
            expect(res).to.have.status(200)
            expect(res.body).to.have.property('email',emailLuuk) 
        })
        it('DELETE /user/:id Should delete user with id', async function(){
            const emailLuuk = "luuk-bartels@hotmail.com";

            const testUser = await new User({
                email:emailLuuk,
                password:"LuukIsAmazing34" ,
                firstName:"Luuk",
                lastName: "Bartels",
                about: "This is a great bio!",
                dateOfBirth:"1997-02-16",
                role:"ADMIN"
            }).save()

            const res = await requester.delete(`/user/${testUser._id}`)
            expect(res).to.have.status(204)
            const user = await User.findOne({email: emailLuuk})
            expect(user).to.be.null
        })
        it('PUT /user/:id Should update user with id', async function(){
            const emailLuuk = "luuk-bartels@hotmail.com";
            const emailUpdate = "luuk-cool@hotmail.com";

            const testUser = await new User({
                email:emailLuuk,
                password:"LuukIsAmazing34" ,
                firstName:"Luuk",
                lastName: "Bartels",
                about: "This is a great bio!",
                dateOfBirth:"1997-02-16",
                role:"ADMIN"
            }).save()
        
            const res = await requester.put(`/user/${testUser._id}`).send({
                email:emailUpdate,
                password:"LuukIsAmazing34",
                firstName:"Luuk",
                lastName: "Bartels",
                about: "This is a great bio!",
                dateOfBirth:"1997-02-16",
                role:"ADMIN"
            })
            expect(res).to.have.status(204)
            const user = await User.findOne({email: emailUpdate})
            expect(user).to.have.property('email', emailUpdate)
        })
    })
})