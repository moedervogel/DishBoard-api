const express = require('express')
const router = express.Router()

const RecipeBook = require('../models/RecipeBook.model')();  // note we need to call the model caching function
const User = require('../models/user.model')()
const CrudController = require('../controllers/crud')
const UserControllerHelper = require('../controllers/user.controller.helper');
const RecipeBookControllerHelper = require('../controllers/recipeBook.controller.helper');

const RecipeBookCrudController = new CrudController(RecipeBook)
const UserControllerHelperClass = new UserControllerHelper(User);
const RecipeBookCrudConrollerHelper = new RecipeBookControllerHelper(RecipeBook)

// create a Recipe Book
router.post('/', UserControllerHelperClass.validateToken, RecipeBookCrudConrollerHelper.addToNeo4j, RecipeBookCrudController.create)

router.post('/follow/:id', UserControllerHelperClass.validateToken, RecipeBookCrudConrollerHelper.followRecipeBook)

router.post('/unfollow/:id', RecipeBookCrudConrollerHelper.unFollowRecipeBook)
// get all Recipe Book
router.get('/', RecipeBookCrudController.getAll)

router.get('/follow/:id', RecipeBookCrudConrollerHelper.getFollowingRecipeBook)

// get a Recipe Book
router.get('/:id', RecipeBookCrudController.getOne)

router.get('/user/:id', RecipeBookCrudConrollerHelper.getRecipeBookFromUserId)

// update a Recipe Book
router.put('/:id', UserControllerHelperClass.validateToken, RecipeBookCrudController.update)

// remove a Recipe Book
router.delete('/:id', UserControllerHelperClass.validateToken, RecipeBookCrudController.delete)

module.exports = router