const chai = require('chai');
const expect = chai.expect;

const requester = require('../../requester.spec')
const User = require('../models/user.model')()
const Ingredient = require('../models/ingredient.model')()

describe('Recipe endpoints', async function() {
    this.beforeEach(async () => {
        // drop both collections before each test
        await Promise.all([User.deleteMany()])
    })
    let testUserId
    let token
    // drop both collections before each test
    const res = await requester.post('/user/register').send({
        email:"luuk-bartels@hotmail.com",
        password:"LuukIsAmazing34" ,
        firstName:"Luuk",
        lastName: "Bartels",
        about: "This is a great bio!",
        dateOfBirth:"1997-02-16",
        role:"ADMIN"
    })
    token = res.body.token
    testUserId = res.body.user._id

    describe('integration tests', function(){
        it('POST /ingredient Should create recipe', async function(){
            const res = await requester.post('/ingredient').set({
                "Authorization": `Bearer ${token}`
            }).send({
                name:"Tomato",
                amount:2,
                size:"Each",
                })
            
            console.log("Res Body",res.body)
            expect(res).to.have.status(201)
            const Ingredient = await Ingredient.findOne({name: "Tomato"})
            expect(Ingredient).to.have.property('name', "Tomato")    
                
        })
        it('GET /ingredient Should create recipe', async function(){
            const nameFirst = "Tomato";
            const nameSecond = "Potato";

            await new Ingredient({
                name:"Tomato",
                amount:2,
                size:"Each",
            }).save()

            await new Ingredient({
                name:"Potato",
                amount:2,
                size:"Each",
            }).save()

            const res = await requester.get('/ingredient')
            expect(res).to.have.status(200)
            const ingredients = res.body;
            expect(ingredients[0]).to.have.property('name',nameFirst)
            expect(ingredients[1]).to.have.property('name',nameSecond)
        })
        it('GET /ingredient/:id Should create recipe', async function(){
            const nameFirst = "Tomato";

            const testingredient = await new Ingredient({
                name:"Tomato",
                amount:2,
                size:"Each",
            }).save()

            const res = await requester.get(`/ingredient/${testingredient._id}`)
            expect(res).to.have.status(200)
            expect(res.body).to.have.property('name',nameFirst) 
        })
        it('PUT /ingredient/:id Should create recipe', async function(){
            const nameFirst = "Tomato";
            const nameEdited = "Potato";

            const testingredient = await new Ingredient({
                name:nameFirst,
                amount:2,
                size:"Each",
            }).save()

            const res = await requester.put(`/ingredient/${testingredient._id}`).set({
                "Authorization": `Bearer ${token}`
            }).send({
                name:"Potato",
                amount:2,
                size:"Each"
            })
            expect(res).to.have.status(204)
            const ingredient = await Ingredient.findOne({name:nameEdited})
            expect(ingredient).to.have.property('name', nameEdited)
        })

        it('DELETE /ingredient/:id Should create recipe', async function(){
            const nameFirst = "Tomato";

            const testingredient = await new Ingredient({
                name:nameFirst,
                amount:2,
                size:"Each",
            }).save()

            const res = await requester.delete(`/ingredient/${testingredient._id}`).set({
                "Authorization": `Bearer ${token}`
            })
            expect(res).to.have.status(204)
            const ingredient = await Ingredient.findOne({name: nameFirst})
            expect(ingredient).to.be.null

        })




    })


})