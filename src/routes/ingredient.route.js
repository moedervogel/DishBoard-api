const express = require('express')
const router = express.Router()

const Ingredient = require('../models/ingredient.model')();  // note we need to call the model caching function
const User = require('../models/user.model')()
const CrudController = require('../controllers/crud')
const UserControllerHelper = require('../controllers/user.controller.helper');

const IngredientCrudController = new CrudController(Ingredient)
const UserControllerHelperClass = new UserControllerHelper(User);


// create a ingredients
router.post('/', UserControllerHelperClass.validateToken, IngredientCrudController.create)

//Get all ingredients
router.get('/', IngredientCrudController.getAll)

//Get specific ingredients
router.get('/:id', IngredientCrudController.getOne)

//Update specific ingredients
router.put('/:id', UserControllerHelperClass.validateToken, IngredientCrudController.update)

// remove a ingredients
router.delete('/:id', UserControllerHelperClass.validateToken, IngredientCrudController.delete)


module.exports = router