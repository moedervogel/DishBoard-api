const chai = require('chai');
const expect = chai.expect;

const requester = require('../../requester.spec')
const User = require('../models/user.model')()
const Recipe = require('../models/recipe.model')()

describe('Recipe endpoints', async function() {
    this.beforeEach(async () => {
        // drop both collections before each test
        await Promise.all([User.deleteMany()])
    })
    let testUserId
    let token
    // drop both collections before each test
    const res = await requester.post('/user/register').send({
        email:"luuk-bartels@hotmail.com",
        password:"LuukIsAmazing34" ,
        firstName:"Luuk",
        lastName: "Bartels",
        about: "This is a great bio!",
        dateOfBirth:"1997-02-16",
        role:"ADMIN"
    })
    token = res.body.token
    testUserId = res.body.user._id
    

    describe('integration tests', function(){
        it('POST /recipe Should create recipe', async function(){
            
            const res = await requester.post('/recipe').set({
                "Authorization": `Bearer ${token}`
            }).send({
                name:"gestoofde groentes",
                time:20,
                forAmountOfPeople:2,
                description:["Test","lekker hoor"],
                user:"619e5501385941dcbfa0dfe7",
                imgUrl:"https://meer-weten-over-eten.nl/wp-content/blogs.dir/13/files/2018/09/Ratatouille.jpg", 
                ingredients:["619e55s385xdcbfa0b7","61f550a8594scbfa0dfe7"]
                })
            console.log("Res Body",res.body)
            expect(res).to.have.status(201)
            const recipe = await Recipe.findOne({name: "gestoofde groentes"})
            expect(recipe).to.have.property('name', "gestoofde groentes")
        })

        it('POST /recipe Should not create when description is lower then 1', async function(){
            const res = await requester.post('/recipe').set({
                "Authorization": `Bearer ${token}`
            }).send({
                name:"gestoofde groentes",
                time:20,
                forAmountOfPeople:2,
                description:[],
                user:"619e5501385941dcbfa0dfe7",
                imgUrl:"https://meer-weten-over-eten.nl/wp-content/blogs.dir/13/files/2018/09/Ratatouille.jpg", 
                ingredients:["619e55s385xdcbfa0b7","61f550a8594scbfa0dfe7"]
                })
            
            expect(res).to.have.status(400)
            const recipe = await Recipe.findOne({name: "gestoofde groentes"})
            expect(recipe).to.be.null
        })

        it('POST /recipe Should not create when Ingredient is lower then 2', async function(){
            const res = await requester.post('/recipe').set({
                "Authorization": `Bearer ${token}`
            }).send({
                name:"gestoofde groentes",
                time:20,
                forAmountOfPeople:2,
                description:["Test","lekker hoor"],
                user:"619e5501385941dcbfa0dfe7",
                imgUrl:"https://meer-weten-over-eten.nl/wp-content/blogs.dir/13/files/2018/09/Ratatouille.jpg", 
                ingredients:[]
                })
            
                console.log(res.body)

            expect(res).to.have.status(400)
            const recipe = await Recipe.findOne({name: "gestoofde groentes"})
            expect(recipe).to.be.null
        })
        
        it('GET /recipe', async function(){
            const nameFirst = "gestoofde groentes";
            const nameSecond = "gestoofde Appels";

            await new Recipe({
                name:nameFirst,
                time:20,
                forAmountOfPeople:2,
                description:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla congue orci quis dignissim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur convallis enim vel est tristique, vitae fermentum nisl viverra. In pretium dolor diam, vel facilisis mi ultrices sagittis. Nam congue vitae libero vel mollis. Curabitur lobortis molestie turpis, ut congue metus gravida vel. Vivamus finibus commodo massa sed blandit. Mauris dictum erat at interdum lobortis.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum semper lacus sit amet sodales luctus. Phasellus lacus nunc, fringilla sed tincidunt vitae, tempus vitae dui. Cras metus lectus, efficitur eu fringilla ac, scelerisque sit amet libero. Nam maximus finibus sagittis. Aliquam sodales ac dui in imperdiet. Donec vel urna id ipsum tincidunt finibus ut nec nibh. Suspendisse non nulla elit. Etiam interdum finibus turpis, id accumsan sapien semper ut. Nulla tortor ante, commodo faucibus sollicitudin vel, sagittis at nibh. Morbi dapibus volutpat sapien a sodales. Proin pulvinar nibh vel magna pellentesque tincidunt. Pellentesque ac risus sapien. Curabitur quis massa sapien. Duis commodo hendrerit mauris vitae ultrices.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum semper lacus sit amet sodales luctus. Phasellus lacus nunc, fringilla sed tincidunt vitae, tempus vitae dui. Cras metus lectus, efficitur eu fringilla ac, scelerisque sit amet libero. Nam maximus finibus sagittis. Aliquam sodales ac dui in imperdiet. Donec vel urna id ipsum tincidunt finibus ut nec nibh. Suspendisse non nulla elit. Etiam interdum finibus turpis, id accumsan sapien semper ut. Nulla tortor ante, commodo faucibus sollicitudin vel, sagittis at nibh. Morbi dapibus volutpat sapien a sodales. Proin pulvinar nibh vel magna pellentesque tincidunt. Pellentesque ac risus sapien. Curabitur quis massa sapien. Duis commodo hendrerit mauris vitae ultrices.","lekker hoor"],
                user:"619e5501385941dcbfa0dfe7",
                imgUrl:"https://meer-weten-over-eten.nl/wp-content/blogs.dir/13/files/2018/09/Ratatouille.jpg", 
                ingredients:["619e55s385xdcbfa0b7","61f550a8594scbfa0dfe7"]
            }).save()

            await new Recipe({
                name:nameSecond,
                time:20,
                forAmountOfPeople:2,
                description:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla congue orci quis dignissim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur convallis enim vel est tristique, vitae fermentum nisl viverra. In pretium dolor diam, vel facilisis mi ultrices sagittis. Nam congue vitae libero vel mollis. Curabitur lobortis molestie turpis, ut congue metus gravida vel. Vivamus finibus commodo massa sed blandit. Mauris dictum erat at interdum lobortis.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum semper lacus sit amet sodales luctus. Phasellus lacus nunc, fringilla sed tincidunt vitae, tempus vitae dui. Cras metus lectus, efficitur eu fringilla ac, scelerisque sit amet libero. Nam maximus finibus sagittis. Aliquam sodales ac dui in imperdiet. Donec vel urna id ipsum tincidunt finibus ut nec nibh. Suspendisse non nulla elit. Etiam interdum finibus turpis, id accumsan sapien semper ut. Nulla tortor ante, commodo faucibus sollicitudin vel, sagittis at nibh. Morbi dapibus volutpat sapien a sodales. Proin pulvinar nibh vel magna pellentesque tincidunt. Pellentesque ac risus sapien. Curabitur quis massa sapien. Duis commodo hendrerit mauris vitae ultrices.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum semper lacus sit amet sodales luctus. Phasellus lacus nunc, fringilla sed tincidunt vitae, tempus vitae dui. Cras metus lectus, efficitur eu fringilla ac, scelerisque sit amet libero. Nam maximus finibus sagittis. Aliquam sodales ac dui in imperdiet. Donec vel urna id ipsum tincidunt finibus ut nec nibh. Suspendisse non nulla elit. Etiam interdum finibus turpis, id accumsan sapien semper ut. Nulla tortor ante, commodo faucibus sollicitudin vel, sagittis at nibh. Morbi dapibus volutpat sapien a sodales. Proin pulvinar nibh vel magna pellentesque tincidunt. Pellentesque ac risus sapien. Curabitur quis massa sapien. Duis commodo hendrerit mauris vitae ultrices.","lekker hoor"],
                user:"619e5501385941dcbfa0dfe7",
                imgUrl:"https://meer-weten-over-eten.nl/wp-content/blogs.dir/13/files/2018/09/Ratatouille.jpg", 
                ingredients:["619e55s385xdcbfa0b7","61f550a8594scbfa0dfe7"]
            }).save()

            const res = await requester.get('/recipe')
            expect(res).to.have.status(200)
            const recipes = res.body;
            expect(recipes[0]).to.have.property('name',nameFirst)
            expect(recipes[1]).to.have.property('name',nameSecond)
        })
        it('GET /recipe:id', async function(){
            const nameFirst = "gestoofde groentes";

            const testRecipe = await new Recipe({
                name:nameFirst,
                time:20,
                forAmountOfPeople:2,
                description:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla congue orci quis dignissim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur convallis enim vel est tristique, vitae fermentum nisl viverra. In pretium dolor diam, vel facilisis mi ultrices sagittis. Nam congue vitae libero vel mollis. Curabitur lobortis molestie turpis, ut congue metus gravida vel. Vivamus finibus commodo massa sed blandit. Mauris dictum erat at interdum lobortis.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum semper lacus sit amet sodales luctus. Phasellus lacus nunc, fringilla sed tincidunt vitae, tempus vitae dui. Cras metus lectus, efficitur eu fringilla ac, scelerisque sit amet libero. Nam maximus finibus sagittis. Aliquam sodales ac dui in imperdiet. Donec vel urna id ipsum tincidunt finibus ut nec nibh. Suspendisse non nulla elit. Etiam interdum finibus turpis, id accumsan sapien semper ut. Nulla tortor ante, commodo faucibus sollicitudin vel, sagittis at nibh. Morbi dapibus volutpat sapien a sodales. Proin pulvinar nibh vel magna pellentesque tincidunt. Pellentesque ac risus sapien. Curabitur quis massa sapien. Duis commodo hendrerit mauris vitae ultrices.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum semper lacus sit amet sodales luctus. Phasellus lacus nunc, fringilla sed tincidunt vitae, tempus vitae dui. Cras metus lectus, efficitur eu fringilla ac, scelerisque sit amet libero. Nam maximus finibus sagittis. Aliquam sodales ac dui in imperdiet. Donec vel urna id ipsum tincidunt finibus ut nec nibh. Suspendisse non nulla elit. Etiam interdum finibus turpis, id accumsan sapien semper ut. Nulla tortor ante, commodo faucibus sollicitudin vel, sagittis at nibh. Morbi dapibus volutpat sapien a sodales. Proin pulvinar nibh vel magna pellentesque tincidunt. Pellentesque ac risus sapien. Curabitur quis massa sapien. Duis commodo hendrerit mauris vitae ultrices.","lekker hoor"],
                user:"619e5501385941dcbfa0dfe7",
                imgUrl:"https://meer-weten-over-eten.nl/wp-content/blogs.dir/13/files/2018/09/Ratatouille.jpg", 
                ingredients:["619e55s385xdcbfa0b7","61f550a8594scbfa0dfe7"]
            }).save()

            const res = await requester.get(`/recipe/${testRecipe._id}`)
            expect(res).to.have.status(200)
            expect(res.body).to.have.property('name',nameFirst) 
        })
        it('PUT /recipe:id', async function(){
            const nameFirst = "gestoofde groentes";
            const nameEdited = "Kip Kerrie";

            const testRecipe = await new Recipe({
                name:nameFirst,
                time:20,
                forAmountOfPeople:2,
                description:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla congue orci quis dignissim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur convallis enim vel est tristique, vitae fermentum nisl viverra. In pretium dolor diam, vel facilisis mi ultrices sagittis. Nam congue vitae libero vel mollis. Curabitur lobortis molestie turpis, ut congue metus gravida vel. Vivamus finibus commodo massa sed blandit. Mauris dictum erat at interdum lobortis.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum semper lacus sit amet sodales luctus. Phasellus lacus nunc, fringilla sed tincidunt vitae, tempus vitae dui. Cras metus lectus, efficitur eu fringilla ac, scelerisque sit amet libero. Nam maximus finibus sagittis. Aliquam sodales ac dui in imperdiet. Donec vel urna id ipsum tincidunt finibus ut nec nibh. Suspendisse non nulla elit. Etiam interdum finibus turpis, id accumsan sapien semper ut. Nulla tortor ante, commodo faucibus sollicitudin vel, sagittis at nibh. Morbi dapibus volutpat sapien a sodales. Proin pulvinar nibh vel magna pellentesque tincidunt. Pellentesque ac risus sapien. Curabitur quis massa sapien. Duis commodo hendrerit mauris vitae ultrices.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum semper lacus sit amet sodales luctus. Phasellus lacus nunc, fringilla sed tincidunt vitae, tempus vitae dui. Cras metus lectus, efficitur eu fringilla ac, scelerisque sit amet libero. Nam maximus finibus sagittis. Aliquam sodales ac dui in imperdiet. Donec vel urna id ipsum tincidunt finibus ut nec nibh. Suspendisse non nulla elit. Etiam interdum finibus turpis, id accumsan sapien semper ut. Nulla tortor ante, commodo faucibus sollicitudin vel, sagittis at nibh. Morbi dapibus volutpat sapien a sodales. Proin pulvinar nibh vel magna pellentesque tincidunt. Pellentesque ac risus sapien. Curabitur quis massa sapien. Duis commodo hendrerit mauris vitae ultrices.","lekker hoor"],
                user:"619e5501385941dcbfa0dfe7",
                imgUrl:"https://meer-weten-over-eten.nl/wp-content/blogs.dir/13/files/2018/09/Ratatouille.jpg", 
                ingredients:["619e55s385xdcbfa0b7","61f550a8594scbfa0dfe7"]
            }).save()
            const res = await requester.put(`/recipe/${testRecipe._id}`).set({
                "Authorization": `Bearer ${token}`
            }).send({
                name:nameEdited,
                time:20,
                forAmountOfPeople:2,
                description:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla congue orci quis dignissim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur convallis enim vel est tristique, vitae fermentum nisl viverra. In pretium dolor diam, vel facilisis mi ultrices sagittis. Nam congue vitae libero vel mollis. Curabitur lobortis molestie turpis, ut congue metus gravida vel. Vivamus finibus commodo massa sed blandit. Mauris dictum erat at interdum lobortis.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum semper lacus sit amet sodales luctus. Phasellus lacus nunc, fringilla sed tincidunt vitae, tempus vitae dui. Cras metus lectus, efficitur eu fringilla ac, scelerisque sit amet libero. Nam maximus finibus sagittis. Aliquam sodales ac dui in imperdiet. Donec vel urna id ipsum tincidunt finibus ut nec nibh. Suspendisse non nulla elit. Etiam interdum finibus turpis, id accumsan sapien semper ut. Nulla tortor ante, commodo faucibus sollicitudin vel, sagittis at nibh. Morbi dapibus volutpat sapien a sodales. Proin pulvinar nibh vel magna pellentesque tincidunt. Pellentesque ac risus sapien. Curabitur quis massa sapien. Duis commodo hendrerit mauris vitae ultrices.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum semper lacus sit amet sodales luctus. Phasellus lacus nunc, fringilla sed tincidunt vitae, tempus vitae dui. Cras metus lectus, efficitur eu fringilla ac, scelerisque sit amet libero. Nam maximus finibus sagittis. Aliquam sodales ac dui in imperdiet. Donec vel urna id ipsum tincidunt finibus ut nec nibh. Suspendisse non nulla elit. Etiam interdum finibus turpis, id accumsan sapien semper ut. Nulla tortor ante, commodo faucibus sollicitudin vel, sagittis at nibh. Morbi dapibus volutpat sapien a sodales. Proin pulvinar nibh vel magna pellentesque tincidunt. Pellentesque ac risus sapien. Curabitur quis massa sapien. Duis commodo hendrerit mauris vitae ultrices.","lekker hoor"],
                user:"619e5501385941dcbfa0dfe7",
                imgUrl:"https://meer-weten-over-eten.nl/wp-content/blogs.dir/13/files/2018/09/Ratatouille.jpg", 
                ingredients:["619e55s385xdcbfa0b7","61f550a8594scbfa0dfe7"]
            })

            expect(res).to.have.status(204)
            const recipe = await Recipe.findOne({name:nameEdited})
            expect(recipe).to.have.property('name', nameEdited)

        })
        it('DELETE /recipe:id', async function(){
            const nameFirst = "gestoofde groentes";

            const testRecipe = await new Recipe({
                name:nameFirst,
                time:20,
                forAmountOfPeople:2,
                description:["Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur fringilla congue orci quis dignissim. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur convallis enim vel est tristique, vitae fermentum nisl viverra. In pretium dolor diam, vel facilisis mi ultrices sagittis. Nam congue vitae libero vel mollis. Curabitur lobortis molestie turpis, ut congue metus gravida vel. Vivamus finibus commodo massa sed blandit. Mauris dictum erat at interdum lobortis.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum semper lacus sit amet sodales luctus. Phasellus lacus nunc, fringilla sed tincidunt vitae, tempus vitae dui. Cras metus lectus, efficitur eu fringilla ac, scelerisque sit amet libero. Nam maximus finibus sagittis. Aliquam sodales ac dui in imperdiet. Donec vel urna id ipsum tincidunt finibus ut nec nibh. Suspendisse non nulla elit. Etiam interdum finibus turpis, id accumsan sapien semper ut. Nulla tortor ante, commodo faucibus sollicitudin vel, sagittis at nibh. Morbi dapibus volutpat sapien a sodales. Proin pulvinar nibh vel magna pellentesque tincidunt. Pellentesque ac risus sapien. Curabitur quis massa sapien. Duis commodo hendrerit mauris vitae ultrices.","Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum semper lacus sit amet sodales luctus. Phasellus lacus nunc, fringilla sed tincidunt vitae, tempus vitae dui. Cras metus lectus, efficitur eu fringilla ac, scelerisque sit amet libero. Nam maximus finibus sagittis. Aliquam sodales ac dui in imperdiet. Donec vel urna id ipsum tincidunt finibus ut nec nibh. Suspendisse non nulla elit. Etiam interdum finibus turpis, id accumsan sapien semper ut. Nulla tortor ante, commodo faucibus sollicitudin vel, sagittis at nibh. Morbi dapibus volutpat sapien a sodales. Proin pulvinar nibh vel magna pellentesque tincidunt. Pellentesque ac risus sapien. Curabitur quis massa sapien. Duis commodo hendrerit mauris vitae ultrices.","lekker hoor"],
                user:"619e5501385941dcbfa0dfe7",
                imgUrl:"https://meer-weten-over-eten.nl/wp-content/blogs.dir/13/files/2018/09/Ratatouille.jpg", 
                ingredients:["619e55s385xdcbfa0b7","61f550a8594scbfa0dfe7"]
            }).save()
            const res = await requester.delete(`/recipe/${testRecipe._id}`).set({
                "Authorization": `Bearer ${token}`
            })
            expect(res).to.have.status(204)
            const recipe = await Recipe.findOne({name: nameFirst})
            expect(recipe).to.be.null
        })
    })
})