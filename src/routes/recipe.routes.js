const express = require('express')
const router = express.Router()

const Recipe = require('../models/recipe.model')() // note we need to call the model caching function
const User = require('../models/user.model')()
const CrudController = require('../controllers/crud')
const UserControllerHelper = require('../controllers/user.controller.helper');

const RecipeCrudController = new CrudController(Recipe)
const UserControllerHelperClass = new UserControllerHelper(User);

const RecipeControllerHelperClass = require('../controllers/recipe.controller.helper')


// create a Recipe
router.post('/', UserControllerHelperClass.validateToken, RecipeControllerHelperClass.validateRecipeDescriptionAndIngredient, RecipeCrudController.create)

// get all Recipe
router.get('/', RecipeCrudController.getAll)

// get a Recipe
router.get('/:id', RecipeCrudController.getOne)

// update a Recipe
router.put('/:id', UserControllerHelperClass.validateToken, RecipeCrudController.update)

// remove a Recipe
router.delete('/:id', UserControllerHelperClass.validateToken, RecipeCrudController.delete)

module.exports = router