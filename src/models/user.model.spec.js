const chai = require('chai');
const expect = chai.expect;

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const User = require('./user.model')()

describe('User model', function(){
    describe('unit tests', function(){
        it('Should reject missing email', async function(){
            const testUser = new User({
                    password:"LuukIsAmazing34",
                    firstName:"Luuk",
                    lastName: "Bartels",
                    about: "This is a great bio!",
                    dateOfBirth:"1997-02-16",
                    role:"ADMIN"
            })
            await expect(testUser.save()).to.be.rejectedWith(Error);
        })
        it('Should reject missing password', async function(){
            const testUser = new User({
                    email: "luuk-bartels@hotmail.com",
                    firstName:"Luuk",
                    lastName: "Bartels",
                    about: "This is a great bio!",
                    dateOfBirth:"1997-02-16",
                    role:"ADMIN"
            })
            await expect(testUser.save()).to.be.rejectedWith(Error);
        })
        it('Should reject missing firstName', async function(){
            const testUser = new User({
                    email: "luuk-bartels@hotmail.com",
                    password:"LuukIsAmazing34" ,
                    lastName: "Bartels",
                    about: "This is a great bio!",
                    dateOfBirth:"1997-02-16",
                    role:"ADMIN"
            })
            await expect(testUser.save()).to.be.rejectedWith(Error);
        })
        it('Should reject missing lastName', async function(){
            const testUser = new User({
                    email: "luuk-bartels@hotmail.com",
                    password:"LuukIsAmazing34" ,
                    firstName:"Luuk",
                    about: "This is a great bio!",
                    dateOfBirth:"1997-02-16",
                    role:"ADMIN"
            })
            await expect(testUser.save()).to.be.rejectedWith(Error);
        })
        it('Should reject missing dateOfBirth', async function(){
            const testUser = new User({
                    email: "luuk-bartels@hotmail.com",
                    password:"LuukIsAmazing34" ,
                    firstName:"Luuk",
                    lastName: "Bartels",
                    about: "This is a great bio!",
                    role:"ADMIN"
            })
            await expect(testUser.save()).to.be.rejectedWith(Error);
        })
        it('Should reject missing role', async function(){
            const testUser = new User({
                    email: "luuk-bartels@hotmail.com",
                    password:"LuukIsAmazing34" ,
                    firstName:"Luuk",
                    lastName: "Bartels",
                    about: "This is a great bio!",
                    dateOfBirth:"1997-02-16",
            })
            await expect(testUser.save()).to.be.rejectedWith(Error);
        })
        it('Should create', async function(){
            const testUser = new User({
                    email: "luuk-bartels@hotmail.com",
                    password:"LuukIsAmazing34" ,
                    firstName:"Luuk",
                    lastName: "Bartels",
                    about: "This is a great bio!",
                    dateOfBirth:"1997-02-16",
                    role:"ADMIN"
            })
            await testUser.save();
			let count = await User.find().countDocuments();
			expect(count).to.equal(1);

        })

        it('Should not create, duplicate email found', async function () {
			await new User({
                email: "luuk-bartels@hotmail.com",
                password:"LuukIsAmazing34" ,
                firstName:"Luuk",
                lastName: "Bartels",
                about: "This is a great bio!",
                dateOfBirth:"1997-02-16",
                role:"ADMIN"
			}).save();
			
			const user = new User({
                email: "luuk-bartels@hotmail.com",
                password:"LuukIsAmazing34" ,
                firstName:"Luuk",
                lastName: "Bartels",
                about: "This is a great bio!",
                dateOfBirth:"1997-02-16",
                role:"ADMIN"
			});

			await expect(user.save()).to.be.rejectedWith(Error);

			let count = await User.find().countDocuments();
			expect(count).to.equal(1);
		});
    })
})