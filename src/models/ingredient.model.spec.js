const chai = require('chai');
const expect = chai.expect;

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const Ingredient = require('./ingredient.model')()

describe('Recipe model', function(){
    describe('unit tests', function(){
        it('Should reject missing name', async function(){
            const testIngredient = new Ingredient({
                amount:2,
                size:"Each",
            })
            await expect(testIngredient.save()).to.be.rejectedWith(Error);
        })
        it('Should reject missing amount', async function(){
            const testIngredient = new Ingredient({
                name:"Tomato",
                size:"Each",
            })
            await expect(testIngredient.save()).to.be.rejectedWith(Error);
        })
        it('Should reject missing size', async function(){
            const testIngredient = new Ingredient({
                name:"Tomato",
                amount:2,
            })
            await expect(testIngredient.save()).to.be.rejectedWith(Error);
        })
        it('Should create Ingredient', async function(){
            const testIngredient = new Ingredient({
                name:"Tomato",
                amount:2,
                size:"Each",
            })
            await testIngredient.save();
            let count = await Ingredient.find().countDocuments();
            expect(count).to.equal(1);
        })
    })
})