const chai = require('chai');
const expect = chai.expect;

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const Recipe = require('./recipe.model')()
const IngredientSchema = require('./ingredient.model');
const NutritionSchema = require('./nutrition.model')

describe('Recipe model', function(){
    describe('unit tests', function(){
        it('Should reject missing name', async function(){
            const testRecipe = new Recipe({
                    time:20,
                    forAmountOfPeople:2,
                    description:["Test","lekker hoor"],
                    user:"619e5501385941dcbfa0dfe7",
                    imgUrl:"https://meer-weten-over-eten.nl/wp-content/blogs.dir/13/files/2018/09/Ratatouille.jpg", 
                    ingredients:["619e55s385xdcbfa0b7","61f550a8594scbfa0dfe7"]
            })
            await expect(testRecipe.save()).to.be.rejectedWith(Error);
        })
        it('Should reject missing time', async function(){
            const testRecipe = new Recipe({
                    name:"gestoofde groentes",
                    forAmountOfPeople:2,
                    description:["Test","lekker hoor"],
                    user:"619e5501385941dcbfa0dfe7",
                    imgUrl:"https://meer-weten-over-eten.nl/wp-content/blogs.dir/13/files/2018/09/Ratatouille.jpg", 
                    ingredients:["619e55s385xdcbfa0b7","61f550a8594scbfa0dfe7"]
            })
            await expect(testRecipe.save()).to.be.rejectedWith(Error);
        })
        it('Should reject missing forAmountOfPeople', async function(){
            const testRecipe = new Recipe({
                    name:"gestoofde groentes",
                    time:20,
                    description:["Test","lekker hoor"],
                    user:"619e5501385941dcbfa0dfe7",
                    imgUrl:"https://meer-weten-over-eten.nl/wp-content/blogs.dir/13/files/2018/09/Ratatouille.jpg", 
                    ingredients:["619e55s385xdcbfa0b7","61f550a8594scbfa0dfe7"]
            })
            await expect(testRecipe.save()).to.be.rejectedWith(Error);
        })
        it('Should reject missing description', async function(){
            const testRecipe = new Recipe({
                    name:"gestoofde groentes",
                    time:20,
                    forAmountOfPeople:2,
                    user:"619e5501385941dcbfa0dfe7",
                    imgUrl:"https://meer-weten-over-eten.nl/wp-content/blogs.dir/13/files/2018/09/Ratatouille.jpg", 
                    ingredients:["619e55s385xdcbfa0b7","61f550a8594scbfa0dfe7"]
            })
            await expect(testRecipe.save()).to.be.rejectedWith(Error);
        })
        it('Should reject description empty', async function(){
            const testRecipe = new Recipe({
                    name:"gestoofde groentes",
                    time:20,
                    forAmountOfPeople:2,
                    description:[],
                    user_:"619e5501385941dcbfa0dfe7",
                    imgUrl:"https://meer-weten-over-eten.nl/wp-content/blogs.dir/13/files/2018/09/Ratatouille.jpg", 
                    ingredients:["619e55s385xdcbfa0b7","61f550a8594scbfa0dfe7"]
            })
            await expect(testRecipe.save()).to.be.rejectedWith(Error);
        })
        it('Should reject missing user_id', async function(){
            const testRecipe = new Recipe({
                    name:"gestoofde groentes",
                    time:20,
                    forAmountOfPeople:2,
                    description:["Test","lekker hoor"],
                    imgUrl:"https://meer-weten-over-eten.nl/wp-content/blogs.dir/13/files/2018/09/Ratatouille.jpg", 
                    ingredients:["619e55s385xdcbfa0b7","61f550a8594scbfa0dfe7"]
            })
            await expect(testRecipe.save()).to.be.rejectedWith(Error);
        })
        it('Should reject missing ingredients', async function(){
            const testRecipe = new Recipe({
                    name:"gestoofde groentes",
                    time:20,
                    forAmountOfPeople:2,
                    description:["Test","lekker hoor"],
                    imgUrl:"https://meer-weten-over-eten.nl/wp-content/blogs.dir/13/files/2018/09/Ratatouille.jpg", 
                    user_id:"619e5501385941dcbfa0dfe7",
            })
            await expect(testRecipe.save()).to.be.rejectedWith(Error);
        })
        it('Should reject ingredients smaller then 2', async function(){
            const testRecipe = new Recipe({
                    name:"gestoofde groentes",
                    time:20,
                    forAmountOfPeople:2,
                    description:["Test","lekker hoor"],
                    imgUrl:"https://meer-weten-over-eten.nl/wp-content/blogs.dir/13/files/2018/09/Ratatouille.jpg", 
                    user:"619e5501385941dcbfa0dfe7",
                    ingredients:["619e55s385xdcbfa0b7"]
            })
            await expect(testRecipe.save()).to.be.rejectedWith(Error);
        })
        it('Should Create Recipe', async function(){
            const testRecipe = new Recipe({
                name:"gestoofde groentes",
                time:20,
                forAmountOfPeople:2,
                description:["Test","lekker hoor"],
                user:"619e5501385941dcbfa0dfe7",
                imgUrl:"https://meer-weten-over-eten.nl/wp-content/blogs.dir/13/files/2018/09/Ratatouille.jpg", 
                ingredients:["619e55s385xdcbfa0b7","61f550a8594scbfa0dfe7"]
            })
            await testRecipe.save();
            let count = await Recipe.find().countDocuments();
            expect(count).to.equal(1);
        })
    })
})