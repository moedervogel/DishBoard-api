const chai = require("chai");
const expect = chai.expect;

var chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);

const RecipeBook = require("./recipeBook.model")();

describe("Recipe model", function () {
  describe("unit tests", function () {
    it("Should reject missing title", async function () {
      const testRecipeBook = new RecipeBook({
        about: "this is a cool recipe Book",
        chapters: [
          {
            chapterTitle: "10 potatoes",
            chapterIntro: "a story about potatoes",
            recipeList: ["619er5013y941dcbfs0dfe7", "e19er5013y921dcbfs1dfe7"],
          },
          {
            chapterTitle: "20 potatoes",
            chapterIntro: "a story about more potatoes",
            recipeList: [
              "619e5b50ax594sdcbfa0dfe7",
              "619e55x01a85w41scbfa0dfe7",
            ],
          },
        ],
        user: "619e5501385941dcbfa0dfe7",
      });
      await expect(testRecipeBook.save()).to.be.rejectedWith(Error);
    });
    it("Should reject missing chapters", async function () {
      const testRecipeBook = new RecipeBook({
        title: "100 potatoes",
        about: "this is a cool recipe Book",
        user: "619e5501385941dcbfa0dfe7",
      });
      await expect(testRecipeBook.save()).to.be.rejectedWith(Error);
    });
    it("Should reject empty chapters", async function () {
      const testRecipeBook = new RecipeBook({
        title: "100 potatoes",
        about: "this is a cool recipe Book",
        chapters: [],
        user: "619e5501385941dcbfa0dfe7",
      });
      await expect(testRecipeBook.save()).to.be.rejectedWith(Error);
    });
    it("Should reject missing chapter title", async function () {
      const testRecipeBook = new RecipeBook({
        title: "100 potatoes",
        about: "this is a cool recipe Book",
        chapters: [
          {
            chapterIntro: "a story about potatoes",
            recipeList: ["619er5013y941dcbfs0dfe7", "e19er5013y921dcbfs1dfe7"],
          },
          {
            chapterIntro: "a story about more potatoes",
            recipeList: [
              "619e5b50ax594sdcbfa0dfe7",
              "619e55x01a85w41scbfa0dfe7",
            ],
          },
        ],
        user: "619e5501385941dcbfa0dfe7",
      });
      await expect(testRecipeBook.save()).to.be.rejectedWith(Error);
    });
    it("Should reject missing chapter recipeList", async function () {
      const testRecipeBook = new RecipeBook({
        title: "100 potatoes",
        about: "this is a cool recipe Book",
        chapters: [
          {
            chapterTitle: "10 potatoes",
            chapterIntro: "a story about potatoes",
          },
          {
            chapterTitle: "20 potatoes",
            chapterIntro: "a story about more potatoes",
          },
        ],
        user: "619e5501385941dcbfa0dfe7",
      });
      await expect(testRecipeBook.save()).to.be.rejectedWith(Error);
    });
    it("Should reject missing user", async function () {
      const testRecipeBook = new RecipeBook({
        title: "100 potatoes",
        about: "this is a cool recipe Book",
        chapters: [
          {
            chapterTitle: "10 potatoes",
            chapterIntro: "a story about potatoes",
            recipeList: ["619er5013y941dcbfs0dfe7", "e19er5013y921dcbfs1dfe7"],
          },
          {
            chapterTitle: "20 potatoes",
            chapterIntro: "a story about more potatoes",
            recipeList: [
              "619e5b50ax594sdcbfa0dfe7",
              "619e55x01a85w41scbfa0dfe7",
            ],
          },
        ],
      });
      await expect(testRecipeBook.save()).to.be.rejectedWith(Error);
    });
    it("Should save Recipe Book", async function () {
      const testRecipeBook = new RecipeBook({
        title: "100 potatoes",
        about: "this is a cool recipe Book",
        chapters: [
          {
            chapterTitle: "10 potatoes",
            chapterIntro: "a story about potatoes",
            recipeList: ["619er5013y941dcbfs0dfe7", "e19er5013y921dcbfs1dfe7"],
          },
          {
            chapterTitle: "20 potatoes",
            chapterIntro: "a story about more potatoes",
            recipeList: ["619e5b50ax594sdcbfa0dfe7","619e55x01a85w41scbfa0dfe7"],
          },
        ],
        user: "619e5501385941dcbfa0dfe7",
      });
      await testRecipe.save();
      let count = await Recipe.find().countDocuments();
      expect(count).to.equal(1);
    });
  });
});
