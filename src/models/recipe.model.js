const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const getModel = require('./model_cache');
const User = require('./user.model')() 
const Ingredient = require('./ingredient.model')();

const RecipeSchema = new Schema({
    name:{
        type: String,
        required:true,
    },
    time:{
        type:Number,
        required:true,
    },
    forAmountOfPeople:{
        type:Number,
        required:true,
    },
    description:[{
        type:String,
        required:true,
    }],
    tips:{
        type:String,
    },
    user: {
        type:Schema.Types.ObjectId,
        ref:"User",
        required:true,
    },
    imgUrl: {
        type:String,
        required:true,
    },
    ingredients:[{
        type:Schema.Types.ObjectId,
        ref:"Ingredient",
        required:true,
    }],
}, {
    toObject:{virtuals:true},
    toJSON:{virtuals:true}
});

function validatorDescription(val){
    return val.length >  0;
}

function validatorIngredients(val){
    return val.length >  1;
}

RecipeSchema.path('description').validate(validatorDescription, 'Array needs to be bigger then 0')
RecipeSchema.path('ingredients').validate(validatorIngredients, 'Array needs to be bigger then 1')


module.exports = getModel('Recipe', RecipeSchema);
 