const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const getModel = require('./model_cache');
const User = require('./user.model')() 
const Recipe = require('./recipe.model')();

const RecipeBookSchema = new Schema({
    title:{
        type:String,
        required:true,
    },
    about:{
        type:String,
    },
    chapters:[{
        chapterTitle:{
            type:String,
            required:true,
        },
        chapterIntro:{
            type:String,
        },
        recipeList:[{
            type:Schema.Types.ObjectId,
            ref:"Recipe",
            required:true,
        }],
    }],
    user: {
        type:Schema.Types.ObjectId,
        ref:"User",
        required:true,
    },
});

function validateMinOne(val){
    return val.length >  0;
}

RecipeBookSchema.path('chapters').validate(validateMinOne, 'Array needs to be bigger then 1')
RecipeBookSchema.path('chapters.recipeList').validate(validateMinOne, 'Array needs to be bigger then 1')

module.exports = getModel('RecipeBook', RecipeBookSchema);
