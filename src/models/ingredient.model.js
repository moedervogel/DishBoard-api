const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const getModel = require('./model_cache');

const IngredientSchema = new Schema({
    name:{
        type: String,
        required:true,
    },
    amount:{
        type:Number,
        required:true,
    },
    size:{
        type:String,
        required:true,
    },
    substitute:[{
        type:Schema.Types.ObjectId,
        ref:'Ingredient',
        required:false,
    }],

});

module.exports = getModel('Ingredient', IngredientSchema);

 