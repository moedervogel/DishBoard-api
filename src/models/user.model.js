const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const getModel = require('./model_cache');

const UserSchema = new Schema({
    email:{
        type:String,
        required:true,
        unique:[true,"User needs to have a unique e-mail"]
    },
    password:{
        type: String,
        required: true
    },
    firstName:{
        type: String,
        required:true,
    },
    lastName:{
        type:String,
        required:true,
    },
    about:{
        type:String,
    },
    dateOfBirth:{
        type:Date,
        required:true,
    },
    role:{
        type:String,
        required:true,
    }

});

module.exports = getModel('User', UserSchema);
 